import { PrimeSamplePage } from './app.po';

describe('prime-sample App', () => {
  let page: PrimeSamplePage;

  beforeEach(() => {
    page = new PrimeSamplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
